﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace MvcApplication1.Models
{
    public class GuestResponse
    {
        [Required(ErrorMessage="por favor ingrese su nombre")]
        public string Name { get; set; }
        [Required(ErrorMessage="por favor entra tu direccion de correo electronico")]
        [RegularExpression(".+\\@.+\\..+", ErrorMessage="ingresa una direccion valida")]
        public string Email { get; set; }
        [Required(ErrorMessage="por favor ingrese su numero de telefono")]
        public string Phone { get; set; }
        [Required(ErrorMessage="por favor indicar si asistira")]
        public bool? WillAttend { get; set; }
    }
}